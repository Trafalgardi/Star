using System;
using System.Collections.Generic;

public class ObjectPool<T>
{
    private readonly Func<T> _creator;
    private readonly Stack<T> _objs = new Stack<T>();

    public ObjectPool(Func<T> creator) => _creator = creator;

    public T Rent() => _objs.Count > 0 ? _objs.Pop() : _creator();
    
    public void Return(T obj) => _objs.Push(obj);
}
