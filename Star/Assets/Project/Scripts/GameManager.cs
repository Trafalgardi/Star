using System;
using System.Collections;
using UnityEngine;

namespace Project.Scripts
{
    public class GameManager : MonoBehaviour
    {
        #region Singleton

        private static GameManager _instance;

        public static GameManager Instance => 
            _instance == null ? _instance = FindObjectOfType<GameManager>() : _instance;

        #endregion
        
        [SerializeField] private StarSpawner _spawner;
        [SerializeField] private StarRaycaster _raycaster;

        private const string BEST_SCORE = "BEST_SCORE";
        
        private void Awake()
        {
            BestScore = PlayerPrefs.GetInt(BEST_SCORE, 0);

            _raycaster.enabled = false;
            
            GeneralPanelManager.Instance.OpenPanel<MainPanel>();
        }

        public int BestScore { get; private set; }
        public int Score { get; private set; }
        public float Timer { get; private set; }
        
        public event Action ScoreUpdated;
        
        public void StartGame()
        {
            Score = 0;
            
            Timer = 10;
            _gameRoutine = StartCoroutine(GameRoutine());
            
            _spawner.StartSpawnCycle();
            _raycaster.enabled = true;
            
            GeneralPanelManager.Instance.OpenPanel<HUDPanel>();
        }

        public void PauseGame()
        {
            Time.timeScale = 0;
            
            _raycaster.enabled = false;
            
            GeneralPanelManager.Instance.OpenPanel<PausePanel>();
        }
        
        public void ResumeGame()
        {
            Time.timeScale = 1;
            
            _raycaster.enabled = true;
            
            GeneralPanelManager.Instance.OpenPanel<HUDPanel>();
        }

        public void EndGame()
        {
            if (Score > BestScore)
            {
                BestScore = Score;
                PlayerPrefs.SetInt(BEST_SCORE, BestScore);
            }
            
            StopCoroutine(_gameRoutine);

            _spawner.StopSpawnCycle();
            _raycaster.enabled = false;
            
            GeneralPanelManager.Instance.OpenPanel<MainPanel>();
        }

        public void IncrementScore()
        {
            Score++;
            ScoreUpdated?.Invoke();
        }

        #region Game Routine

        private Coroutine _gameRoutine;

        private WaitForEndOfFrame _waitForEndOfFrame = new WaitForEndOfFrame();

        private IEnumerator GameRoutine()
        {
            while ((Timer -= Time.deltaTime) > 0f) yield return _waitForEndOfFrame;
            EndGame();
        }

        #endregion
    }
}
