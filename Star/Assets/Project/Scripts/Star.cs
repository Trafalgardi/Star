﻿using System.Collections;
using UnityEngine;

namespace Project.Scripts
{
    public class Star : MonoBehaviour
    {
        [SerializeField] private BoxCollider2D  _collider;
        [SerializeField] private GameObject     _model;
        [SerializeField] private ParticleSystem _particleSystem;
        
        private float _endPositionY;
        private float _fallVelocity;
        private float _rotateVelocity;

        private StarController _controller;
        
        public void OnRent(Vector3 startPosition, float endPositionY, float fallVelocity, float rotateVelocity, StarController controller)
        {
            transform.position = startPosition;
            _endPositionY = endPositionY;
            
            _fallVelocity = fallVelocity;
            _rotateVelocity = rotateVelocity;

            IsClickable = true;
            
            _controller = controller;
        }

        public void OnReturn()
        {
            _controller = null;
        }

        public void Click()
        {
            _controller.OnClick(this);
            IsClickable = false;
            _particleSystem.Play();
            StartCoroutine(DeathRoutine());
        }

        private bool IsClickable
        {
            get => _collider.enabled;
            set => _model.SetActive(_collider.enabled = value);
        }

        private void Update()
        {
            if (IsClickable == false) return;
            
            transform.Translate(_fallVelocity * Time.deltaTime * Vector3.down, Space.World);
            transform.Rotate(_rotateVelocity * Time.deltaTime * Vector3.forward, Space.World);

            if (transform.position.y >= _endPositionY) return;
            
            IsClickable = false;
            _controller.OnDeath(this);
        }

        private IEnumerator DeathRoutine()
        {
            yield return new WaitForSeconds(_particleSystem.main.duration);
            _controller.OnDeath(this);
        }
    }
}