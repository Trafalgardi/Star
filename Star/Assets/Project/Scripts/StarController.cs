﻿using UnityEngine;

namespace Project.Scripts
{
    public class StarController : MonoBehaviour
    {
        [SerializeField] private StarSpawner _spawner;
        
        public void OnClick(Star star)
        {
            GameManager.Instance.IncrementScore();
        }
        
        public void OnDeath(Star star)
        {
            _spawner.ReturnStar(star);
        }
    }
}