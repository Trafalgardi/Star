﻿using UnityEngine;

namespace Project.Scripts
{
    public class StarRaycaster : MonoBehaviour
    {
        [SerializeField] private Camera _camera;

        private void Update()
        {
            if (Input.GetButtonDown("Fire1") == false) return;
            
            var collider = Physics2D.Raycast(_camera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero).collider;
            
            if (collider == null) return;
            
            var star = collider.gameObject.GetComponent<Star>();
            
            if (star == null) return;
            
            star.Click();
        }
    }
}