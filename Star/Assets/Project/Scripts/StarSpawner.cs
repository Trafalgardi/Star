﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Project.Scripts
{
    public class StarSpawner : MonoBehaviour
    {
        [SerializeField] private Star           _prefab;
        [SerializeField] private Transform      _container;
        [SerializeField] private Camera         _camera;

        [SerializeField] private StarController _controller;

        private ObjectPool<Star> _pool;
        
        private float _posXMin;
        private float _posXMax;
        private float _posYMin;
        private float _posYMax;

        private void Awake()
        {
            _pool = new ObjectPool<Star>(() => Instantiate(_prefab, _container));

            #region Game Zone Setup

            // bottom left
            var bl = _camera.ScreenToWorldPoint(Vector3.zero);
            // top right
            var tr = _camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
            
            var starHalfSize = _prefab.GetComponent<BoxCollider2D>().size / 2;

            _posXMin = bl.x + starHalfSize.x;
            _posXMax = tr.x - starHalfSize.x;
            _posYMin = bl.y - starHalfSize.y;
            _posYMax = tr.y + starHalfSize.y;

            #endregion
        }

        private void RentStar()
        {
            var star = _pool.Rent();
            star.gameObject.SetActive(true);
            
            star.OnRent(
                new Vector3(Random.Range(_posXMin, _posXMax), _posYMax),
                _posYMin,
                Random.Range(1f, 5f),
                (Random.value < 0.5f ? -1 : +1) * Random.Range(20f, 80f),
                _controller);
        }
        
        public void ReturnStar(Star star)
        {
            star.OnReturn();
            star.gameObject.SetActive(false);
            _pool.Return(star);
        }

        #region Spawn Cycle

        public void StartSpawnCycle()
        {
            foreach (Transform child in _container)
            {
                if (child.gameObject.activeSelf == false) continue;
                ReturnStar(child.GetComponent<Star>());
            }
            
            _spawnCycleRoutine = StartCoroutine(SpawnCycleRoutine());
        }
        
        public void StopSpawnCycle()
        {
            StopCoroutine(_spawnCycleRoutine);
        }
        
        private Coroutine _spawnCycleRoutine;
        
        private IEnumerator SpawnCycleRoutine()
        {
            while(true)
            {
                RentStar();
                yield return new WaitForSeconds(Random.Range(0.1f, 1f));
            }
        }

        #endregion
    }
}