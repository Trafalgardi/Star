using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuVFX : MonoBehaviour
{
    #region Singleton

    private static MainMenuVFX _mInstance;

    public static MainMenuVFX Instance
    {
        get
        {
            if (_mInstance == null)
            {
                _mInstance = FindObjectOfType<MainMenuVFX>();
            }

            return _mInstance;
        }
    }


    #endregion

    [SerializeField] private List<ParticleSystem> _particleSystems;

    public void StartParticle()
    {
        for (int i = 0; i < _particleSystems.Count; i++)
        {
            _particleSystems[i].Play();
        }
    }

    public void StopParticle()
    {
        for (int i = 0; i < _particleSystems.Count; i++)
        {
            _particleSystems[i].Stop();
        } 
    }
}
