using System;
using BurningKnight.PanelManager;
using Project.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HUDPanel : PanelBase
{
    [SerializeField] private Button          _pauseButton;
    [SerializeField] private TextMeshProUGUI _timer;
    [SerializeField] private TextMeshProUGUI _score;

    private Action _updateScore;
    private UnityAction _pauseGame;
    
    private int _lastTimerValue;

    public override void Init()
    {
        _updateScore = UpdateScore;
        _pauseGame = GameManager.Instance.PauseGame;
    }

    protected override void OnOpened()
    {
        UpdateScore();
        
        GameManager.Instance.ScoreUpdated += _updateScore;
        
        _pauseButton.onClick.AddListener(_pauseGame);
    }

    protected override void OnClosed()
    {
        GameManager.Instance.ScoreUpdated -= _updateScore;
        
        _pauseButton.onClick.RemoveListener(_pauseGame);
    }

    private void UpdateScore()
    {
        _score.text = GameManager.Instance.Score.ToString();
    }
    
    private void Update()
    {
        var timerValue = (int) GameManager.Instance.Timer + 1;
        if (timerValue == _lastTimerValue) return;
        _lastTimerValue = timerValue;
        _timer.text = _lastTimerValue.ToString();
    }
}
