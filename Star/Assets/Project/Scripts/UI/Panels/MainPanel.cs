using BurningKnight.PanelManager;
using Project.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainPanel : PanelBase
{
    [SerializeField] private TextMeshProUGUI _bestScore;
    [SerializeField] private Button          _startButton;

    private UnityAction startGame;

    public override void Init()
    {
        startGame = GameManager.Instance.StartGame;
    }

    protected override void OnOpened()
    {
        _bestScore.text = GameManager.Instance.BestScore.ToString();
        
        _startButton.onClick.AddListener(startGame);
        MainMenuVFX.Instance.StartParticle();
    }

    protected override void OnClosed()
    {
        _startButton.onClick.RemoveListener(startGame);
        MainMenuVFX.Instance.StopParticle();
    }
}