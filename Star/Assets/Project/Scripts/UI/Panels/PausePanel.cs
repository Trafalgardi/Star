using BurningKnight.PanelManager;
using Project.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PausePanel : PanelBase
{
    [SerializeField] private TextMeshProUGUI _score;
    [SerializeField] private TextMeshProUGUI _bestScore;
    [SerializeField] private Button _unPauseButton;

    private UnityAction _resumeGame;
    private UnityAction _endGame;

    public override void Init()
    {
        _resumeGame = GameManager.Instance.ResumeGame;
        _endGame = GameManager.Instance.EndGame;
    }

    protected override void OnOpened()
    {
        _bestScore.text = GameManager.Instance.BestScore.ToString();
        _score.text = GameManager.Instance.Score.ToString();
        
        _unPauseButton.onClick.AddListener(_resumeGame);
    }

    protected override void OnClosed()
    {
        _unPauseButton.onClick.RemoveListener(_resumeGame);
    }
}
